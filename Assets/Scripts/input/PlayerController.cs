using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private InputPlayerController _inputPlayerController;
    private Rigidbody _playerRigidbody;
    private Vector3 _startPosition;
    
    [SerializeField]
    private float moveSpeed = 600f;
    [SerializeField]
    private float turboModifier = 2f;

    private void Awake()
    {
        _inputPlayerController = new InputPlayerController();
        _playerRigidbody = GetComponent<Rigidbody>();
        _startPosition = transform.position;

        _inputPlayerController.car.resetPosition.performed += context => ResetPosition();
    }

    private void OnEnable()
    {
        _inputPlayerController.Enable();
    }
    
    private void Disable()
    {
        _inputPlayerController.Disable();
    }

    private void Move(Vector2 direction)
    {
        float speed = Speed();
        _playerRigidbody.velocity = new Vector3(direction.x * speed * Time.fixedDeltaTime, 0f,
            direction.y * speed * Time.fixedDeltaTime);
    }

    private float Speed()
    {
        float speed = moveSpeed;
        if (_inputPlayerController.car.Turbo.inProgress)
        {
            speed *= turboModifier;
        }
        return speed;
    }
    private void FixedUpdate()
    {
        Vector2 moveDirection = _inputPlayerController.car.moveWASD.ReadValue<Vector2>();
        moveDirection += _inputPlayerController.car.moveArrows.ReadValue<Vector2>();
        moveDirection += _inputPlayerController.car.androidGamepad.ReadValue<Vector2>();
        Move(moveDirection.normalized);
    }

    private void ResetPosition()
    {
        _playerRigidbody.MovePosition(_startPosition);
        _playerRigidbody.MoveRotation(Quaternion.identity);
    }
}
